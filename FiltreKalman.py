#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 14:10:49 2020

@author: zkamil
"""
import pandas as pd
from numpy import *
from numpy.linalg import inv

def chosing_data_file(file_name):
    read_csv = pd.read_csv(file_name, sep=";")
    return(read_csv)

#IMPLEMENTATION OF KALMAN'S FILTER
#PREDICTION STEP
#X : The mean state estimate of the previous step (k −1).
#P : The state covariance of previous step (k −1).
#A : The transition n × n matrix.
#Q : The process noise covariance matrix.
#B : The input effect matrix.
#U : The control input.

def kf_predict(X, P, A, Q, B, U):
    X = dot(A, X) + dot(B, U)
    P = dot(A, dot(P, A.T)) + Q
    return(X,P)

#UPDATE STEP
#K : the Kalman Gain matrix
#IM : the Mean of predictive distribution of Y
#IS : the Covariance or predictive mean of Y
#LH : the Predictive probability (likelihood) of measurement which is
#computed using the Python function gauss_pdf.

def kf_update(X, P, Y, H, R):
    IM = dot(H, X)
    IS = R + dot(H, dot(P, H.T))
    K = dot(P, dot(H.T, inv(IS)))
    X = X + dot(K, (Y-IM))
    P = P - dot(K, dot(IS, K.T))
    LH = gauss_pdf(Y, IM, IS)
    return (X,P,K,IM,IS,LH)

def gauss_pdf(X, M, S):
    if shape(M)[1] == 1:
        DX = X - tile(M, shape(X)[0])
        E = 0.5 * sum(DX * (dot(inv(S), DX)), axis=0)
        E = E + 0.5 * shape(M)[0] * log(2 * pi) + 0.5 * log(linalg.det(S))
        P = exp(-E)
    elif shape(X)[1] == 1:
        DX = tile(X, shape(M)[1])- M
        E = 0.5 * sum(DX * (dot(inv(S), DX)), axis=0)
        E = E + 0.5 * M.shape()[0] * log(2 * pi) + 0.5 * log(linalg.det(S))
        P = exp(-E)
    else:
        DX = X-M
        E = 0.5 * dot(DX.T, dot(inv(S), DX))
        E = E + 0.5 * shape(M)[0] * log(2 * pi) + 0.5 * log(linalg.det(S))
        P = exp(-E)
    return (P[0],E[0])

#Utilisation du filtre de Kalman pour angle de telltale 1 et direction du vent
#ANGLE DE TELLTALE 1
#Initialisation des states matrices

## Function returns the Kalman filtered values of the specified column of the csv
def filtre(N_iter, variable, read_csv):
    dt = read_csv['timestamp'][1] - read_csv['timestamp'][0]

    X = array([[0.0],[0.0],[0.1],[0.1]])
    P = diag((0.01, 0.01, 0.01, 0.01))
    A = array([[1, 0, dt, 0], [0, 1, 0, dt], [0, 0, 1, 0], [0, 0, 0, 1]])
    Q = eye(shape(X)[0])
    B = eye(shape(X)[0])
    U = zeros((shape(X)[0], 1))

    # Matrices de mesures
    Y = [read_csv[variable][0]]
    H = array([[1, 0, 0, 0]])
    R = eye(shape(Y)[0])
    time = []
    timeMin = []
    measures = []
    estimatedMeasures = []

    for i in range(0, N_iter):
        (X,P) = kf_predict(X, P, A, Q, B, U)
        (X, P, K, IM, IS, LH) = kf_update(X, P, Y, H, R)
        Y =[read_csv[variable][i]]
        time.append(dt*i)
        if i%10 == 0:
            measures.append(Y[0])
        estimatedMeasures.append(X[0,0])

    for i in range(0, N_iter//100):
        timeMin.append(time[i*100])

    return(time, measures, timeMin, estimatedMeasures)
