# DATA ANALYSE

 This programm allows us to treat the incoming datas and then display it using a PyQt5 interface

## Prerequisite

 - PyQt5
 - Spyder with Python 3.7

## Installation

 - PyQt5 : run pip3 install PyQt5
 - Spyder with Python 3.7 : Go on https://www.anaconda.com/products/individual

## How to launch the project
 Steps for using the project :
   - Run the mainInterface.py file
   - Enter the name of the csv file (for the moment it only works with penons.csv)
   - Click on the Display button
   - Chose on the ComboBox which data you want to display with the colorbar (for the moment it only works with the first two ones)
   
