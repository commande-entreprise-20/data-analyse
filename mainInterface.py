##Created by Kamil Mahmal and Jake Smedley
#Last updated 8/12/20

from PyQt5.QtWidgets import QTextEdit, QGridLayout, QGraphicsItem, QGraphicsView, QGraphicsScene, QWidget, QApplication, QPushButton, QMessageBox,QVBoxLayout, QHBoxLayout, QLabel, QGroupBox, QComboBox, QLineEdit, QCheckBox, QFileDialog, QProgressBar
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import time
import sys
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import pandas as pd
from numpy import *
from numpy.linalg import inv
import FiltreKalman as k_f

# This class contains the interface 
class mainWindow(QWidget):
    #define class variables, outside init function means they can be accessed globally
    N_iter = 10000
    file = "penons.csv"
    read_csv = k_f.chosing_data_file(file)
    

    def __init__(self, parent=None):

        QWidget.__init__(self, parent)

        #Window Parameters
        self.setWindowTitle("Interface Analyse des données récupérées")
        self.resize(1500, 1000)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        
        #Variables
        self.chose_axis = 0
        
        #Creating widgets
        #ComboBox for the colorbar
        self.colorbar_box = QComboBox()
        self.colorbar_box.addItems(["Speed", "Azimuth", "Force", "Angle de roulis", "Angle de tangage"])
        self.colorbar_box.setFixedHeight(35)
        self.colorbar_box.setFixedWidth(800)
        self.colorbar_box.currentIndexChanged.connect(self.colorbar_datas)
        self.currentWidget = speedGraph()

        #Creating Buttons
        #Exit button to quit the application
        self.exit_button = QPushButton('Exit')
        self.exit_button.clicked.connect(self.close)
        #Display Button to display the datas
        self.display_button = QPushButton("Display")
        self.display_button.clicked.connect(self.display_data)

        #LineEdit to change the name of the file
        self.file_name = QLineEdit(self.file)
        self.file_name.returnPressed.connect(self.change_file_name)

        #Colorbar Layout
        self.colorbar_layout = QGridLayout()
        self.colorbar_layout.addWidget(QLabel("Colorbar"), 2, 0, 1, 1)
        self.colorbar_layout.addWidget(self.colorbar_box, 2, 1, 1, 1)

        #Edit layout
        self.edit_layout = QGridLayout()
        self.edit_layout.addWidget(QLabel("File Name :"), 0, 0, 1, 1)
        self.edit_layout.addWidget(self.file_name, 0, 1, 1, 1)

        #Button layout
        self.button_layout = QGridLayout()
        self.button_layout.addWidget(self.exit_button, 0, 0)
        self.button_layout.addWidget(self.display_button, 1, 0)

        #Data Layout
        self.data_layout = QVBoxLayout()
        self.data_layout.addLayout(self.colorbar_layout)

        #Main layout
        self.main_layout = QGridLayout()
        self.main_layout.addLayout(self.button_layout, 2, 0)
        self.main_layout.addLayout(self.edit_layout, 0, 0, 1, 1)
        self.main_layout.addLayout(self.data_layout, 0, 1, 2, 2)
        self.setLayout(self.main_layout)

    #Method used to change the colorbar depending on which datas we wish to plot
    def colorbar_datas(self):
        if self.colorbar_box.currentIndex() == 0:
            self.chose_axis = 0
            self.display_data()
        if self.colorbar_box.currentIndex() == 1:
            self.chose_axis = 1
            self.display_data()
        if self.colorbar_box.currentIndex() == 2:
            self.chose_axis = 2
            self.display_data()
        if self.colorbar_box.currentIndex() == 3:
            self.chose_axis = 3
            self.display_data()
        if self.colorbar_box.currentIndex() == 4:
            self.chose_axis = 4
            self.display_data()
            
    #Method used to change the name of the file
    def change_file_name(self):
        self.file = self.file_name.text()
        self.read_csv = k_f.chosing_data_file(self.file)

    #Method used to delete the current graph and the display the new graph 
    #depending on which datas we wish to display
    def display_data(self):
        self.data_layout.removeWidget(self.currentWidget)
        if self.chose_axis == 0:
            self.currentWidget = speedGraph()
            self.data_layout.addWidget(self.currentWidget)            
        elif self.chose_axis == 1:
            self.currentWidget = angleGraph()
            self.data_layout.addWidget(self.currentWidget)           
    ## To complete with the forceGraph roulisGraph and tangageGraph
    ## We can't use them yet because we don't have the right type of csv file

#This class contains the model for making colorGraph that we will use in order
#to create the different graphs
class colorGraph(FigureCanvas):
    def __init__(self, variable, label):
        #We start by creating the figure along the toolbar in which we will
        #display our datas
        graph = Figure()
        self.axes = graph.add_subplot(111)
        FigureCanvas.__init__(self,graph)
        self.setParent(None)
        self.canvas = FigureCanvas(graph)
        self.toolbar = NavigationToolbar(self.canvas, self)
        
        #We apply the Kalman Filter to the variable we chose with the number 
        #of iterations precised in the mainWindow global variables
        (x, y, x2, y2) = k_f.filtre(mainWindow.N_iter, variable, mainWindow.read_csv)
        (time, measures, time_min, estimated_measures) = k_f.filtre(mainWindow.N_iter, 'WindTrue1_Angle', mainWindow.read_csv)
        
        #With self.c we define the datas displayed with the colorbar
        self.c = y2[0:(mainWindow.N_iter)]
        self.plot = self.axes.scatter(time, estimated_measures, s=2, label='Estimation', c=self.c)
        self.figure.colorbar(self.plot, label = label)
        self.axes.set_ylabel('Y_position')
        self.axes.set_xlabel('X_position')
        self.axes.set_title('Tracking_GPS')
        self.axes.legend()
        

## For each type of graph we want, we create a new class similar to colorGraph ##
## New classes go here ##
# First variable needs to correspond EXACTLY to the title of CSV column
# Second is the label of the graph
class speedGraph(colorGraph):
    def __init__(self):
        colorGraph.__init__(self, 'WindTrue1_Speed', 'Speed m/s')

class angleGraph(colorGraph):
    def __init__(self):
        colorGraph.__init__(self, 'WindTrue1_Angle', 'Angle °')
        
class forceGraph(colorGraph):
    def __init__(self):
        colorGraph.__init__(self, 'Force', 'Force(N)')
        
class roulisGraph(colorGraph):
    def __init__(self):
        colorGraph.__init__(self, 'Angle_Roulis', 'Angle_Roulis °')
        
class tangageGraph(colorGraph):
    def __init__(self):
        colorGraph.__init__(self, 'Angle_Tangage', 'Angle_Tangage °')



app = QApplication(sys.argv)
data_view = mainWindow()
data_view.show()
sys.exit(app.exec_())
